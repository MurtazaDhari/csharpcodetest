using Moq;
using PropertyConfiguration;
using PropertyConfiguration.Models;
using System;
using System.Net.Security;
using Xunit;

namespace PropertyConfigurationUnitTests
{
    public class MetricsTest
    {
        [Fact]
        public void ApartmentTest()
        {
            var model = new Configuration
            {
                Width = 50,
                Length = 100,
                SiteConfiguration = new SiteConfiguration
                {
                    AvgAptArea = 74,
                    DevelopmentType = DevelopmentType.Apartment,
                    NumStoreys = 3,
                    SiteCoverage =  70,
                    
                }
            };


            var service = new PropertyMetricsService();

            var metric = service.GetMetrics(model) as ApartmentMetrics;
            
            Assert.NotNull(metric);
            Assert.Equal(5000, metric.SiteArea);
            Assert.Equal(300, metric.SitePerimeter);
            Assert.Equal(77700000, metric.NoOfApartments);
            Assert.Equal(350000, metric.BuildingFootprint);
            Assert.Equal(1050000, metric.BuildingGfa) ;
            Assert.Equal(DevelopmentType.Apartment, metric.DevelopmentType);
        }

        [Fact]
        public void SubDivisionTest()
        {
            var configuration = new Configuration
            {
                Width = 100,
                Length = 500,
                SiteConfiguration = new SiteConfiguration
                {
                    AvgLotSize = 300,
                    DevelopmentType = DevelopmentType.Subdivision,
                    NumStoreys = 0,
                    SiteCoverage = 90,

                }
            };


            var service = new PropertyMetricsService();

            var metric = service.GetMetrics(configuration) as SubDivisionMetrics;

            Assert.NotNull(metric);
            Assert.Equal(50000, metric.SiteArea);
            Assert.Equal(1200, metric.SitePerimeter);
            Assert.Equal(15000000, metric.NumberOfLots);
            Assert.Equal(DevelopmentType.Subdivision, metric.DevelopmentType);
        }

        [Fact]
        public void CommercialTest()
        {
            var configuration = new Configuration
            {
                Width = 250,
                Length = 700,
                SiteConfiguration = new SiteConfiguration
                {
                    DevelopmentType = DevelopmentType.Commercial,
                    NumStoreys = 20,
                    SiteCoverage = 70,
                    CommericalMix = 20,
                    RetailMix = 70

                }
            };


            var service = new PropertyMetricsService();

            var model = service.GetMetrics(configuration);
            var metric = model as CommercialMetrics;

            Assert.NotNull(metric);
            Assert.Equal(175000, metric.SiteArea);
            Assert.Equal(1900, metric.SitePerimeter);
            Assert.Equal(12250000, metric.BuildingFootprint);
            Assert.Equal(245000000, metric.BuildingGfa);
            Assert.Equal(4900000000, metric.CommercialGfa);
            Assert.Equal(17150000000, metric.RetailGfa);
            Assert.Equal(DevelopmentType.Commercial, metric.DevelopmentType);

            Assert.Null(model as ApartmentMetrics);
        }

        [Fact]
        public void MixedUseTest()
        {
            var model = new Configuration
            {
                Width = 250,
                Length = 700,
                SiteConfiguration = new SiteConfiguration
                {
                    AvgAptArea = 74,
                    DevelopmentType = DevelopmentType.Mixed_Use,
                    NumStoreys = 5,
                    SiteCoverage = 70,
                    CommericalMix = 20,
                    ResidentialMix = 10,
                    RetailMix = 70

                }
            };


            var service = new PropertyMetricsService();

            var metric = service.GetMetrics(model) as MixedUseMetrics;

            Assert.NotNull(metric);
            Assert.Equal(175000, metric.SiteArea);
            Assert.Equal(1900, metric.SitePerimeter);
            Assert.Equal(12250000, metric.BuildingFootprint);
            Assert.Equal(61250000, metric.BuildingGfa);
            Assert.Equal(317275000000, metric.NoOfApartments);
            Assert.Equal(1225000000, metric.CommercialGfa);
            Assert.Equal(4287500000, metric.RetailGfa);
            Assert.Equal(612500000, metric.ResidentialGfa);
            Assert.Equal(DevelopmentType.Mixed_Use, metric.DevelopmentType);
        }
    }
}
