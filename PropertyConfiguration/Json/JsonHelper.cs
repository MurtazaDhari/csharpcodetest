﻿using System.IO;
using System.Text.Json;
using System.Threading.Tasks;

namespace PropertyConfiguration.Json
{
    /// <summary>
    /// Helper class for writing and reading json
    /// </summary>
    public static class JsonHelper
    {
        public static async Task<T> Read<T>(string file)
        {
            T value;
            using (FileStream fs = File.OpenRead(file))
            {
                value = await JsonSerializer.DeserializeAsync<T>(fs);
            }

            return value;
        }

        public static void Write<T>(string file, T[] values) 
            where T : IJsonSerializable
        {
            var writerOption = new JsonWriterOptions
            {
                Indented = true
            };

            using (FileStream fs = File.OpenWrite(file))
            {
                using (var writer = new Utf8JsonWriter(fs, writerOption))
                {
                    writer.WriteStartArray();
                    foreach (var value in values)
                    {
                        writer.Write(value);

                    }
                    writer.WriteEndArray();
                }

            }
        }
    }
}
