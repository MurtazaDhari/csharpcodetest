﻿using System.Text.Json;

namespace PropertyConfiguration.Json
{
    /// <summary>
    /// Custom json serializable for class
    /// </summary>
    public interface IJsonSerializable
    {
        void WriteJson(Utf8JsonWriter writer);
    }
}
