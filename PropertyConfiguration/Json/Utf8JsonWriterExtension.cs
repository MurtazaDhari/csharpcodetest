﻿using System.Text.Json;

namespace PropertyConfiguration.Json
{
    /// <summary>
    /// Extension class for IJsonSerializable
    /// </summary>
    public static class Utf8JsonWriterExtension
    {
        public static Utf8JsonWriter Write(this Utf8JsonWriter writer, IJsonSerializable value)
        {
            if (value != null)
            {
                writer.WriteStartObject();
                value.WriteJson(writer);
                writer.WriteEndObject();
            }

            return writer;
        }
    }
}
