﻿namespace PropertyConfiguration.Config
{
    public class PropertyConfigurationSettings
    {
        public string InputFilePath { get; set; }

        public string OutputFilePath { get; set; }
    }
}
