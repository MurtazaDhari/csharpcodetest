﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
namespace PropertyConfiguration.Config
{
    public static class ContainerConfiguration
    {
        public static ServiceProvider Configure(string [] args)
        {
            var configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetParent(AppContext.BaseDirectory).FullName)
            .AddJsonFile("appsettings.json")
            .AddCommandLine(args)
            .Build();

            return new ServiceCollection()
                .AddLogging(l => l.AddConsole(opt =>
                {
                    opt.DisableColors = true;
                    opt.LogToStandardErrorThreshold = LogLevel.Information;
                }))
                .AddTransient<IPropertyMetricsService, PropertyMetricsService>()
                .Configure<PropertyConfigurationSettings>(configuration.GetSection(nameof(PropertyConfigurationSettings)))
                .BuildServiceProvider();
        }
    }
}
