﻿using System.Text.Json;

namespace PropertyConfiguration.Models
{
    public class CommercialMetrics : BuildingMetrics
    {
        public CommercialMetrics(Configuration model) : base(model)
        {
            Calculate(model);
        }


        public double CommercialGfa { get; set; }

        public double RetailGfa { get; set; }

        public override DevelopmentType DevelopmentType => DevelopmentType.Commercial;

        public override void Calculate(Configuration model)
        {
            CommercialGfa = BuildingGfa * model.SiteConfiguration.CommericalMix;
            RetailGfa = BuildingGfa * model.SiteConfiguration.RetailMix;
        }

        public override void WriteJson(Utf8JsonWriter writer)
        {
            base.WriteJson(writer);
            writer.WriteNumber("commercial_gfa", CommercialGfa);
            writer.WriteNumber("retail_gfa", RetailGfa);
        }
    }
}
