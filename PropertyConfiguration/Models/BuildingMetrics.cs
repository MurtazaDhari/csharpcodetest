﻿using System.Text.Json;

namespace PropertyConfiguration.Models
{
    public abstract class BuildingMetrics : PropertyMetrics
    {
        protected BuildingMetrics(Configuration model) : base(model.Width, model.Length)
        {
            BuildingFootprint = SiteArea * model.SiteConfiguration.SiteCoverage;
            BuildingGfa = BuildingFootprint * model.SiteConfiguration.NumStoreys;
        }

        public double BuildingFootprint { get; set; }

        public double BuildingGfa { get; set; }

        public override void WriteJson(Utf8JsonWriter writer)
        {
            base.WriteJson(writer);
            writer.WriteNumber("building_footprints", BuildingFootprint);
            writer.WriteNumber("building_gfa", BuildingGfa);
        }

    }    
}
