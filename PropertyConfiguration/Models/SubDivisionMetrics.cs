﻿using System.Text.Json;
using System.Text.Json.Serialization;

namespace PropertyConfiguration.Models
{
    public class SubDivisionMetrics : PropertyMetrics
    {
        public SubDivisionMetrics(Configuration model) : base(model.Width, model.Length)
        {
            Calculate(model);
        }

        [JsonPropertyName("num_of_lots")]
        public double NumberOfLots { get; set; }

        public override DevelopmentType DevelopmentType => DevelopmentType.Subdivision;

        public override void Calculate(Configuration model)
        {
            NumberOfLots = SiteArea * model.SiteConfiguration.AvgLotSize;
        }

        public override void WriteJson(Utf8JsonWriter writer)
        {
            base.WriteJson(writer);
            writer.WriteNumber("num_of_lots", NumberOfLots);
        }
    }
}
