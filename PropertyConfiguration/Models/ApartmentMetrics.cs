﻿using System;
using System.Text.Json;

namespace PropertyConfiguration.Models
{
    public class ApartmentMetrics : BuildingMetrics
    {
        public ApartmentMetrics(Configuration model) : base(model)
        {
            Calculate(model);
        }

        public long NoOfApartments { get; set; }

        public override DevelopmentType DevelopmentType => DevelopmentType.Apartment;

        public override void Calculate(Configuration model)
        {
            NoOfApartments = Convert.ToInt64(Math.Round(BuildingGfa * model.SiteConfiguration.AvgAptArea));
        }

        public override void WriteJson(Utf8JsonWriter writer)
        {
            base.WriteJson(writer);
            writer.WriteNumber("no_of_apartments", NoOfApartments);
        }
    }
}
