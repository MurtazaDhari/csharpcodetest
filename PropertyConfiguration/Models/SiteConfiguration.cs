﻿using System.Text.Json.Serialization;
using PropertyConfiguration.Models;

namespace PropertyConfiguration
{
    public class SiteConfiguration
    {
        [JsonPropertyName("num_storeys")]
        public uint NumStoreys { get; set; }

        [JsonPropertyName("site_coverage")]
        public uint SiteCoverage { get; set; }

        [JsonPropertyName("development_type")]
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public DevelopmentType DevelopmentType { get; set; }

        [JsonPropertyName("avg_apt_area")]
        public double AvgAptArea { get; set; }

        [JsonPropertyName("avg_lot_size")]
        public double AvgLotSize { get; set; }

        [JsonPropertyName("commerical_mix")]
        public double CommericalMix { get; set; }

        [JsonPropertyName("retail_mix")]
        public double RetailMix { get; set; }

        [JsonPropertyName("residential_mix")]
        public double ResidentialMix { get; set; }
    }   
}
