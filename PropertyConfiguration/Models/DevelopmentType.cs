﻿namespace PropertyConfiguration.Models
{
    public enum DevelopmentType
    {
        Apartment,
        Commercial,
        Subdivision,
        Mixed_Use
    }
}
