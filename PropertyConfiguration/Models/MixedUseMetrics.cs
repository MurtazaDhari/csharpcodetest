﻿using System.Text.Json;

namespace PropertyConfiguration.Models
{
    public class MixedUseMetrics : BuildingMetrics
    {
        public MixedUseMetrics(Configuration model) : base(model)
        {
            Calculate(model);
        }

        public double NoOfApartments { get; set; }

        public double CommercialGfa { get; set; }

        public double RetailGfa { get; set; }

        public double ResidentialGfa { get; set; }

        public override DevelopmentType DevelopmentType => DevelopmentType.Mixed_Use;

        public override void Calculate(Configuration model)
        {
            CommercialGfa = BuildingGfa * model.SiteConfiguration.CommericalMix;
            RetailGfa = BuildingGfa * model.SiteConfiguration.RetailMix;
            ResidentialGfa = BuildingGfa * model.SiteConfiguration.ResidentialMix;
            NoOfApartments = RetailGfa * model.SiteConfiguration.AvgAptArea;
        }

        public override void WriteJson(Utf8JsonWriter writer)
        {
            base.WriteJson(writer);
            writer.WriteNumber("no_of_apartments", NoOfApartments);
            writer.WriteNumber("commercial_gfa", CommercialGfa);
            writer.WriteNumber("retail_gfa", RetailGfa);
            writer.WriteNumber("residential_gfa", ResidentialGfa);
        }
    }
}
