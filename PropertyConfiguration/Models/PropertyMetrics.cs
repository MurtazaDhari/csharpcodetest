﻿using PropertyConfiguration.Json;
using System.Text.Json;

namespace PropertyConfiguration.Models
{
    public abstract class PropertyMetrics : IJsonSerializable
    {

        public PropertyMetrics(double width, double length)
        {
            SiteArea = width * length;
            SitePerimeter = 2 * (width + length);
        }

        public double SiteArea { get; set; }

        public double SitePerimeter { get; set; }

        public abstract DevelopmentType DevelopmentType { get; }

        public abstract void Calculate(Configuration model);

        public virtual void WriteJson(Utf8JsonWriter writer)
        {
            writer.WriteNumber("site_area", SiteArea);
            writer.WriteNumber("site_perimeter", SitePerimeter);
            writer.WriteString("development_type", DevelopmentType.ToString());
        }
    }
}
