﻿using System.Text.Json.Serialization;

namespace PropertyConfiguration
{
    public class Configuration
    {
        [JsonPropertyName("width")]
        public double Width { get; set; }

        [JsonPropertyName("length")]
        public double Length { get; set; }

        [JsonPropertyName("site_config")]
        public SiteConfiguration SiteConfiguration { get; set; }
    }
}
