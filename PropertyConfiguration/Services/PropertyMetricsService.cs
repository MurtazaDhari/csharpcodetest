﻿using PropertyConfiguration.Models;

namespace PropertyConfiguration
{
    public class PropertyMetricsService : IPropertyMetricsService
    {
        public PropertyMetrics GetMetrics(Configuration configuration)
        {
            switch (configuration.SiteConfiguration.DevelopmentType)
            {
                case DevelopmentType.Subdivision:
                    return new SubDivisionMetrics(configuration);
                case DevelopmentType.Apartment:
                    return new ApartmentMetrics(configuration);
                case DevelopmentType.Commercial:
                    return new CommercialMetrics(configuration);
                case DevelopmentType.Mixed_Use:
                    return new MixedUseMetrics(configuration);
                default: return null;
            }
        }
    }
}
