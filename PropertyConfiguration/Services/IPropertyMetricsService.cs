﻿using PropertyConfiguration.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace PropertyConfiguration
{
    public interface IPropertyMetricsService
    {
        PropertyMetrics GetMetrics(Configuration configuration);
    }
}
