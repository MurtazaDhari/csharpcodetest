﻿using System;
using System.IO;
using System.Threading.Tasks;
using PropertyConfiguration.Config;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using PropertyConfiguration.Models;
using Microsoft.Extensions.Options;
using PropertyConfiguration.Json;

namespace PropertyConfiguration
{
    class Program
    {
        public static async Task Main(string[] args)
        {
            var serviceProvider = ContainerConfiguration.Configure(args);
            var settings = serviceProvider.GetService<IOptions<PropertyConfigurationSettings>>();
            var logger = serviceProvider.GetRequiredService<ILogger<Program>>();

            if (settings?.Value == null || string.IsNullOrWhiteSpace(settings.Value.InputFilePath) || string.IsNullOrWhiteSpace(settings.Value.OutputFilePath))
            {
                logger.LogError("Input/Output filename is not provided");
                return;
            }

            if (!File.Exists(settings.Value.InputFilePath))
            {
                logger.LogError("File does not exists");
                return;
            }

            var properties = await JsonHelper.Read<Configuration[]>(settings.Value.InputFilePath);
            PropertyMetrics[] metrics = new PropertyMetrics[properties.Length];

            var configurationService = serviceProvider.GetService<IPropertyMetricsService>();
            int i = 0;
            foreach(var property in properties)
            {
                var metric = configurationService.GetMetrics(property);
                if (metric != null)
                    metrics[i++] = metric;
            }

            JsonHelper.Write(settings.Value.OutputFilePath, metrics);
            Console.WriteLine("Finished");
        }
    }
}
